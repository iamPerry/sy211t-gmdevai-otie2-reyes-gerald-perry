using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

enum locations
{
    Helipad = 0,
    TwinMountain = 2,
    Barracks = 4,
    CommandCenter = 6,
    Ruins = 9,
    OilRefinery = 10,
    Tankers = 11,
    Radar = 3,
    CommandPost = 8,
    Factory = 12,
    Middle = 5
}

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 20.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2 / .0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject[] buttons;
    GameObject currentNode;
    int currentWaypointIndex = 0;
    Graph graph;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0]; 
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        currentNode = graph.getPathPoint(currentWaypointIndex);

        if(Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        if(currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                            Quaternion.LookRotation(direction),
                                            Time.deltaTime * rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void GoTo()
    {
        string loc = EventSystem.current.currentSelectedGameObject.name;
        for(int i = 0; i< wps.Length; i++)
        {
            var location = (locations)i;
            if (loc == location.ToString())
            {
                graph.AStar(currentNode, wps[(int) (locations)i]);
                currentWaypointIndex = 0;
            }
        }
    }
}
