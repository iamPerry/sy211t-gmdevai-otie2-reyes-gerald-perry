using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectTank : MonoBehaviour
{
    public Rigidbody[] tanks;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setGreenActive()
    {
        tanks[0].velocity = new Vector3(10, 0, 0);
        tanks[1].velocity = new Vector3(0, 0, 0);
        tanks[2].velocity = new Vector3(0, 0, 0);
    }
    public void setRedActive()
    {
        tanks[0].velocity = new Vector3(0, 0, 0);
        tanks[1].velocity = new Vector3(10, 0, 0);
        tanks[2].velocity = new Vector3(0, 0, 0);
    }
    public void setBlueActive()
    {
        tanks[0].velocity = new Vector3(0, 0, 0);
        tanks[1].velocity = new Vector3(0, 0, 0);
        tanks[2].velocity = new Vector3(10, 0, 0);
    }
}
