using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour
{
    public Transform player, pet;


    public float fract = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform);
        //transform.position += transform.forward * speed * Time.deltaTime;
        transform.position = Vector3.Lerp(pet.position, player.position, fract);
    }
}
